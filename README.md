# Versionado

Versión **PHP**: **8.0.25**

Versión **Wordpress**: **6.4.1**

# Ficheros relevantes (ficheros php donde he escrito código)

Estas son:

* Todo los ficheros del plugin de _UsersList_ 
* El archivo __functions.php__ del __tema hijo__ (_twentytwentyfour-child_) 
* El archivo __header.php__ del __tema hijo__ (_twentytwentyfour-child_) 
* El archivo __footer.php__ del __tema hijo__ (_twentytwentyfour-child_) 

# Enlaces

URL de **Gitlab**: **https://gitlab.com/jedahee/wordpress-test-experience**

URL del **plugin** en **local**: **http://localhost/wordpress_test_eXperience/wp-admin/admin.php?page=users-list**

(También puedes acceder desde el panel de admin. de Wordpress)


# Info. del sitio

Titulo del sitio: **Usuarios eXperience**

Nombre de usuario: **admin**

Contraseña del sitio (usuario Owner): __ijnLIDeo)Fl@r^9vU*__

Correo electrónico vinculado a la web: **jdaza.her@gmail.com**

Visibilidad en los motores de búsqueda: **No indexado**


# Info. extra

El archivo .gitignore es solo de ejemplo.

Para que tengáis la certeza de que se como funciona.
