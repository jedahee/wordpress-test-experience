<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

// END ENQUEUE PARENT ACTION

// PETICIONES AJAX REGISTRADAS
add_action( 'wp_ajax_get_users_ex', 'get_users_ex' );
add_action( 'wp_ajax_no_priv_get_users_ex', 'get_users_ex' );

add_action( 'wp_ajax_get_count_users_ex', 'get_count_users_ex' );
add_action( 'wp_ajax_no_priv_get_count_users_ex', 'get_count_users_ex' );

add_action( 'wp_ajax_filter_users_ex', 'filter_users_ex' );
add_action( 'wp_ajax_no_priv_filter_users_ex', 'filter_users_ex' );

add_action( 'wp_ajax_filter_count_users_ex', 'filter_count_users_ex' );
add_action( 'wp_ajax_no_priv_filter_count_users_ex', 'filter_count_users_ex' );

// Devuelve el número de usuarios existentes
function get_count_users_ex() {
    $data = file_get_contents(plugins_url('UsersList') . '/db/users.json'); // Obteniendo todos los usuarios
    $users = json_decode($data)->usuarios; // Lo transformamos a un array de PHP
    echo json_encode(count($users)); // Devolvemos la cantidad de usuarios que hay
    wp_die();
}

// Devuelve el número de usuarios existentes
// en una búsqueda con filtro
function filter_count_users_ex() {

    // Parámetros para filtrar
    $name = isset($_POST["name"]) ? htmlspecialchars($_POST["name"]) : "";
    $surname1 = isset($_POST["surname1"]) ? htmlspecialchars($_POST["surname1"]) : "";
    $surname2 = isset($_POST["surname2"]) ? htmlspecialchars($_POST["surname2"]) : "";
    $email = isset($_POST["email"]) ? htmlspecialchars($_POST["email"]) : "";
    $username = isset($_POST["username"]) ? htmlspecialchars($_POST["username"]) : "";
    
    $data = file_get_contents(plugins_url('UsersList') . '/db/users.json'); // Obtenemos usuarios
    $users = json_decode($data)->usuarios; // Transformamos JSON a un array de PHP
    
    // Si existe valor en algún parámetro...
    if ($name != "" || $surname1 != "" || $surname2 != "" || $username != "" || $email != "") {
        $users_filtered = [];

        foreach($users as $user) {
            // Con similar_text comprobamos el parecido de ambos strings,
            // similar_text devuelve un porcentaje de letras coincidentes.
            similar_text($name, $user->name, $percent_name); 
            similar_text($username, $user->username, $percent_username);
            similar_text($surname1, $user->surname1, $percent_surname1);
            similar_text($surname2, $user->surname2, $percent_surname2);
            similar_text($email, $user->email, $percent_email);

            // Decimos que las letras coincidentes deben ser más del 20%...
            if ($percent_name > 20 || $percent_surname1 > 20 || $percent_surname2 > 20 || $percent_email > 20 || $percent_username > 20) {
                $users_filtered[] = $user;
            }
        }
        
    }

    echo json_encode(count($users_filtered));
    wp_die();
}

// Devuelve los usuarios trás una búsqueda con filtro
function filter_users_ex() {

    // Obteniendo parámetros
    $name = isset($_POST["name"]) ? htmlspecialchars($_POST["name"]) : "";
    $surname1 = isset($_POST["surname1"]) ? htmlspecialchars($_POST["surname1"]) : "";
    $surname2 = isset($_POST["surname2"]) ? htmlspecialchars($_POST["surname2"]) : "";
    $email = isset($_POST["email"]) ? htmlspecialchars($_POST["email"]) : "";
    $username = isset($_POST["username"]) ? htmlspecialchars($_POST["username"]) : "";
	$limit = 5;

    if (isset($_POST["page_ex"])) {
        $limit_end = $_POST["page_ex"] * $limit;
        $limit_start = $limit_end - $limit;
    } else {
        $limit_end = $limit;
        $limit_start = 0;
    }
    
    $data = file_get_contents(plugins_url('UsersList') . '/db/users.json');
    $users = json_decode($data)->usuarios;    
    
    // Si eiste algún parámetro de filtro...
    if ($name != "" || $surname1 != "" || $surname2 != "" || $username != "" || $email != "") {
        $users_filtered = [];

        foreach($users as $user) {
            similar_text($name, $user->name, $percent_name);
            similar_text($username, $user->username, $percent_username);
            similar_text($surname1, $user->surname1, $percent_surname1);
            similar_text($surname2, $user->surname2, $percent_surname2);
            similar_text($email, $user->email, $percent_email);

            if ($percent_name > 10 || $percent_surname1 > 10 || $percent_surname2 > 10 || $percent_email> 10 || $percent_username > 10) {
                $users_filtered[] = $user;
            }
        
        }
        
        // Se corta el array filtrado para la paginación
        if (count($users_filtered) > $limit) {
            $users_page = array_slice($users_filtered, $limit_start, $limit_end);
        }
    } else {
        if (count($users) > $limit) {
            $users_page = array_slice($users, $limit_start, $limit_end);
        }
    }

    echo json_encode($users_page);
    wp_die();

}

// Devuelve todos los usuarios
function get_users_ex() {

	$limit = 5; // establecemos un limite para la paginación

    // Si existe el parámetro para la paginación, lo ajustamos
    // sino, lo inicializamos
    if (isset($_POST["page_ex"])) {
        $limit_end = $_POST["page_ex"] * $limit;
        $limit_start = $limit_end - $limit;
    } else {
        $limit_end = $limit;
        $limit_start = 0;
    }
    
    // Obteniendo json y guardandolo dentro de $data
    $data = file_get_contents(plugins_url('UsersList') . '/db/users.json');
    // Formateando el JSON a la estructura de un array asociativo en PHP
    $users = json_decode($data)->usuarios;    

    // Si los resultados superan el límite por página,
    // se recorta el array
    if (count($users) > $limit) {
        $users_page = array_slice($users, $limit_start, $limit_end);
    }

    echo json_encode($users_page); // Respuesta
    wp_die();

}