<?php

/*
Plugin Name: UsersList
Plugin URI: https://gitlab.com/jedahee
Description: Módulo personalizado para la candidatura en eXperience
Version: 1.0
Author: Jesús Daza Hernández
Author URI: https://github.com/jedahee
License: GPL2
*/


defined('ABSPATH') or die("Por favor, ingrese en la web de una forma válida"); // Por seguridad

// Definiendo constantes con las que se va a trabajar
define('PATH_FILE_EX',plugin_dir_path(__FILE__));
define('PATH_DATA_DB_EX',plugin_dir_path(__FILE__) . "db/users.json");
define('PATH_CSS_EX',plugin_dir_path(__FILE__) . "assets/css/");
define('PATH_JS_EX',plugin_dir_path(__FILE__) . "assets/js/");
define('PATH_IMAGES_EX',plugin_dir_path(__FILE__) . "assets/images/");

// Añadiendo el plugin a la barra de administración de Wordpress
add_action('admin_menu' , 'plugin_add_menu');

function plugin_add_menu(){
    add_menu_page('UsersList | Módulo personalizado'  , 'UsersList'  , 'administrator' , 'users-list' , 'init_page' );
}

//Encolando archivo css y js
// ------------------------
add_action('admin_enqueue_scripts', 'enqueue_styles');

// Encolando css
function enqueue_styles() {
    wp_enqueue_style(
        "styles",
        plugin_dir_url(__DIR__) . 'UsersList/assets/css/styles.css',
        [],
        '1.0.0',
        'all',
    );
}

add_action('admin_enqueue_scripts', 'enqueue_scripts');

// Encolando js
function enqueue_scripts() {
    wp_enqueue_script(
        "main",
        plugin_dir_url(__DIR__) . 'UsersList/assets/js/main.js',
        [],
        '1.0.0',
        true,
    );

    wp_localize_script(
        'main',
        'dcms_vars',
        array(
            'url_ex'=> admin_url('admin-ajax.php'),
        )
    );
}

// Función que va a ejecutar todo el contenido del plugin personalizado.
function init_page(){
        
    // Estructura del plugin (HTML)
    show_html_content();

}


// Función que muestra todo el contenido de la web
function show_html_content() {
 ?> 
     <main class="content">
        <!-- FORMULARIO DE FILTRO -->
        <nav class="filter">
            <form  class="filter__form">
                <legend>Filtra por aquí</legend>

                <div class="name-container">
                    <label for="name">Nombre</label>
                    <input type="text" id="name">
                </div>

                
                <div class="username-container">
                    <label for="username">Nombre de usuario</label>
                    <input type="text" id="username">
                </div>

                <div class="surname1-container">
                    <label for="surname1">Primer apellido</label>
                    <input type="text" id="surname1" >
                </div>

                <div class="surname2-container">
                    <label for="surname2">Segundo apellido</label>
                    <input type="text" id="surname2">
                </div>

                <div class="email-container">
                    <label for="email">Correo electrónico</label>
                    <input type="text" id="email">
                </div>

                <button class="btn-filter" type="submit">
                    Filtrar
                </button>
            </form>
            <!-- FIN FORMULARIO DE FILTRO -->
        </nav>
    
        <!-- TABLA DE USUARIOS -->
        <table class="users-table">
            <thead>
                <th>Nombre</th>
                <th>Nombre de usuario</th>
                <th>Primer apellido</th>
                <th>Segundo apellido</th>
                <th>Correo electrónico</th>
                
            </thead>

            <tbody>
                <!-- (FILAS Y COL) CONTENIDO GENERADO CON JS -->
            </tbody>
            
            <tfoot>
                <div class="paginator">
                    <!-- (PAGINADOR) CONTENIDO GENERADO CON JS -->
                </div>
            </tfoot>
        </table>
        <!-- FIN TABLA DE USUARIOS -->
    </main>
 <?php
}
?>