// Pequeño truco para seleccionar los elementos del DOM
// de forma más cómoda 
$ = (elem) => { return document.querySelector(elem) };
$$ = (elem) => { return document.querySelectorAll(elem) };

window.onload = function(e) {
  // Variables DOM
  $table = $(".users-table");
  $tbody = $(".users-table tbody");
  $paginator = $(".paginator");
  $paginator = $(".paginator");
  $btnFilter = $(".btn-filter");
  
  // Variables de datos
  limit = 5;

  // Obtener usuarios (Sin filtro)
  getUsers(1).then(users => {
    fillTableContent(users); // Rellena la tabla con los datos de los usuarios    
  })

  // Obtener el número de usuarios (Sin filtro)
  getUsersCount().then(users_no => {
    fillPagination(users_no, false); // Rellena la paginación con números
  })

  // Cuando se pulsa sobre el botón de filtrar...
  $btnFilter.addEventListener("click", function(e) {
    e.preventDefault(); // Prevenimos la función del botón
    
    // Comprobamos que los parámetros de filtro no están vacio
    if ($("#name").value != "" || $("#username").value != "" || $("#email").value != "" || $("#surname1").value != "" || $("#surname2").value != "") {
      
      // Obtenemos los usuarios (Con filtro)
      filterUsers(1).then(users => {
        fillTableContent(users); // Rellena la tabla con los datos de los usuarios    
      })

      // Obtener el número de usuarios (Con filtro)
      filterUsersCount().then(users_no => {
        fillPagination(users_no, true); // Rellena la paginación con números   
      })
      
    } else {
      // Obtenemos los usuarios (Sin filtro)
      getUsers(1).then(users => {
        fillTableContent(users); // Rellena la tabla con los datos de los usuarios   
      })
    
      // Obtener el número de usuarios (Sin filtro)
      getUsersCount().then(users_no => {
        fillPagination(users_no, false); // Rellena la paginación con números    
      }) 
    }
    
  })

}

function fillPagination(users_no, is_filter) {
  num = users_no / limit; // Número max de paginaciones

  $paginator.innerHTML = ""; // Vaciamos el contenedor del paginador

  // Ahora,lo rellenamos de nuevo con elementos p
  for (var i = 0; i < num; i++) {
    let p = document.createElement("p");
    p.textContent = (i+1); // Añadimos texto
    
    // Añadimos funcionalidad JS
    p.addEventListener("click", function() {
      if (!is_filter) {

        getUsers(this.textContent).then(users => {
          fillTableContent(users);      
        })
      
      } else {
        filterUsers(this.textContent).then(users => {
          fillTableContent(users);      
        })
      }
      
    })

    $paginator.appendChild(p);
  }
  
}

// Filtra los usuarios por página
async function filterUsers(page) {
  const purl = dcms_vars.url_ex; // Recogemos la URL de admin-ajax

  const formData = new FormData();
  // Añadimos parámetros al formulario de datos
  formData.append( 'action', 'filter_users_ex' );
  formData.append( 'page_ex', page );
  formData.append( 'name', $("#name").value );
  formData.append( 'username', $("#username").value );
  formData.append( 'email', $("#email").value );
  formData.append( 'surname1', $("#surname1").value );
  formData.append( 'surname2', $("#surname2").value );
  
  // Enviamos...
  let res = await fetch(purl, {
    method: 'POST',
    body: formData
  });

  let data = await res.json(); // Recogemos la respuesta y la convertimos a JSON 
  return data;
}

// Cuenta los usuarios filtrados
async function filterUsersCount() {
  const purl = dcms_vars.url_ex;

  const formData = new FormData();
  // Añadimos parámetros al formulario de datos
  formData.append( 'action', 'filter_count_users_ex' );
  formData.append( 'name', $("#name").value );
  formData.append( 'username', $("#username").value );
  formData.append( 'email', $("#email").value );
  formData.append( 'surname1', $("#surname1").value );
  formData.append( 'surname2', $("#surname2").value );usuarios
  
  let res = await fetch(purl, {
    method: 'POST',
    body: formData
  });
  let data = await res.text(); // Recogemos la respuesta y la convertimos a texto
  return data;
}

// Obtiene todos los usuarios (Sin filtro)
async function getUsers(page) {
  const purl = dcms_vars.url_ex;

  const formData = new FormData();
  formData.append( 'action', 'get_users_ex' );
  formData.append( 'page_ex', page ); // Se pasa la paginación

  let res = await fetch(purl, {
    method: 'POST',
    body: formData
  });
  let data = await res.json(); 
  return data;
}

// Cuenta todos los usuarios
async function getUsersCount() {
  const purl = dcms_vars.url_ex;

  const formData = new FormData();
  formData.append( 'action', 'get_count_users_ex' );
  
  let res = await fetch(purl, {
    method: 'POST',
    body: formData
  });
  let data = await res.text(); 
  return data;
}

// Rellena la tabla de  con los usuarios pasados.
function fillTableContent(users) {
  $tbody.innerHTML="";

  if (users != undefined && users.length > 0) {
  
    users.forEach(user => {
      
      // Fila de la tabla
      let tr = document.createElement("tr");
      
      tr.classList.add("user-row");  
      tr.classList.add("user_"+user.id);
      
      // Columnas de la fila
      let td_name = document.createElement("td");
      let td_username = document.createElement("td");
      let td_surname1 = document.createElement("td");
      let td_surname2 = document.createElement("td");
      let td_email = document.createElement("td");
      
      // Añadiendo contenido a las columnas
      td_name.textContent = user.name;
      td_username.textContent = user.username;
      td_email.textContent = user.email;
      td_surname1.textContent = user.surname1;
      td_surname2.textContent = user.surname2;
      
      // Añado las columnas con datos a la fila correspondiente
      tr.appendChild(td_name);
      tr.appendChild(td_username);
      tr.appendChild(td_surname1);
      tr.appendChild(td_surname2);
      tr.appendChild(td_email);

      // Lo añado a la tabla de usuarios
      $tbody.appendChild(tr);
      
    });  
  }
}