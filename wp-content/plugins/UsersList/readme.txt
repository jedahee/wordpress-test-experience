Users List
----------
Este es el módulo personalizado para la candidatura en eXperience.

Trata de traer un listado de usuarios (simulados en un JSON) a través de una petición POST 
y poder realizar las siguientes acciones en estos usuarios:

1. Filtrar a través de un formulario
2. Paginar con AJAX

Espero que quedéis satisfecho con la solución y gracias por la oportunidad.
